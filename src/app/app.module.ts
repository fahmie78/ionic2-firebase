import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddReminderPage } from '../pages/add-reminder/add-reminder';
import { AngularFireModule } from 'angularfire2';
import { ProgressBarComponent } from '../components/progress-bar/progress-bar'
import { PageDetailPage } from '../pages/page-detail/page-detail';

export const firebaseConfig = {
  apiKey: "AIzaSyCUeFJ9wV8RWMWgibDPtfH--YrJJrVh1h4",
  authDomain: "ionic2firebase-2fcfb.firebaseapp.com",
  databaseURL: "https://ionic2firebase-2fcfb.firebaseio.com",
  storageBucket: "ionic2firebase-2fcfb.appspot.com",
  messagingSenderId: "821079518765"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddReminderPage,
    ProgressBarComponent,
    PageDetailPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddReminderPage,
    PageDetailPage
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }]
})
export class AppModule { }
