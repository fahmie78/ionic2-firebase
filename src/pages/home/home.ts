import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { AddReminderPage } from '../add-reminder/add-reminder';
import { AngularFire } from 'angularfire2';
import { Reminder } from '../../classes/reminder';
import { PageDetailPage } from '../page-detail/page-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  shouldShowEmptyState: boolean = true;
  reminderList: Reminder[] = [];

  constructor(public navCtrl: NavController, public af: AngularFire) {

  }

  ionViewDidLoad() {
    let firebaseReminderList = this.af.database.list("/reminders");

    firebaseReminderList.subscribe(data =>
      this.reminderList = data.map((item) => {
        let reminder = Object.assign(new Reminder(), item);
        reminder.key = item.$key;
        if (data.length > 0) { this.shouldShowEmptyState = false }
        return reminder;
      }))

    // firebaseReminderList.subscribe(data => {
    //   this.reminderList = new Array();
    //   if (data.length > 0) { this.shouldShowEmptyState = false }
    //   data.forEach((item)=>{
    //     let reminder = Object.assign(new Reminder(), item);
    //     reminder.key = item.$key;
    //     //console.log(reminder);
    //     this.reminderList.push(reminder);
    //   });
    // });
  }

  navigateToReminder() {
    this.navCtrl.push(AddReminderPage);
  }

  gotoDetail(item: Reminder) {
    this.navCtrl.push(PageDetailPage, item);
  }

}
