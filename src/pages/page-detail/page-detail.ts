import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Reminder } from '../../classes/reminder';
import { AngularFire } from 'angularfire2'
/*
  Generated class for the PageDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-page-detail',
  templateUrl: 'page-detail.html'
})
export class PageDetailPage {

  reminder: Reminder;
  constructor(public navCtrl: NavController, public navParams: NavParams, public af: AngularFire) {
    this.reminder = navParams.data;
    console.log(this.reminder);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PageDetailPage');
  }

  getTodayDate(): Date {
    return new Date();
  }

  deleteReminder() {
    let reminderList = this.af.database.list("/reminders");
    reminderList.remove(this.reminder.key).then(() => {

    }, (error) => {

    });
    this.navCtrl.pop();
  }

}
