import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Reminder } from '../../classes/reminder';
import { AngularFire } from 'angularfire2';

/*
  Generated class for the AddReminder page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-reminder',
  templateUrl: 'add-reminder.html'
})
export class AddReminderPage {

  reminder: Reminder = new Reminder();
  avatarRows = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public af: AngularFire, public loadingController: LoadingController, public toast: ToastController) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddReminderPage');

    var row1 = {
      value: [
        "assets/images/backpack.png",
        "assets/images/ball.png",
        "assets/images/basketball.png",
        "assets/images/bear.png",
        "assets/images/beer.png",
        "assets/images/book.png"
      ]
    }

    var row2 = {
      value: [
        "assets/images/box.png",
        "assets/images/cake.png",
        "assets/images/camera.png",
        "assets/images/cat.png",
        "assets/images/clock.png",
        "assets/images/cloud.png"
      ]
    }

    var row3 = {
      value: [
        "assets/images/game.png",
        "assets/images/gift.png",
        "assets/images/graduation.png",
        "assets/images/mastercard.png",
        "assets/images/moneybag.png",
        "assets/images/movie.png"
      ]
    }

    var row4 = {
      value: [
        "assets/images/rainbow.png",
        "assets/images/rice.png",
        "assets/images/spaghetti.png",
        "assets/images/speech-bubble.png",
        "assets/images/sun.png",
        "assets/images/throphy.png"
      ]
    }

    this.avatarRows.push(row1);
    this.avatarRows.push(row2);
    this.avatarRows.push(row3);
    this.avatarRows.push(row4);
  }

  createReminder() {
    //console.log(this.reminder);

    // if (!this.reminder.hasDuration) {
    //    let today = new Date();
    //    this.reminder.date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + (today.getDay() + 1);
    // }

    let loader = this.loadingController.create({
      content: "Be patient..."
    });

    let ts = this.toast.create({
      duration: 3000
    });

    loader.present();

    let reminderList = this.af.database.list("/reminders");
    reminderList.push(this.reminder).then(() => {
      loader.dismiss();
      this.navCtrl.pop();
    }, (error) => {
      //console.log(error);
      loader.dismiss();
      ts.setMessage(error.message);
      ts.present();
    });
  }

  updateAvatar(path: string) {
    this.reminder.avatar = path;
  }

}
